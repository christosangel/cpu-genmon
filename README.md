# cpu-genmon

A simple bash script to display cpu usage in **xfce DE**. 

It displays  a percent total usage of CPU.
 
 It also recognizes how many cores there are, and displays one bar for each cpu core in the xfce panel. It works in combination with the **genmon** plugin.


On hover over the applet, this tooltip is shown:

![image 1](screenshot1.png)


Clicking on the panel applet opens gnome-system-monitor.

## INSTRUCTIONS

-  Change Directory to cpu-genmon

`$ cd cpu-genmon`

-  Make this file executable with the following command in terminal:

`chmod +x cpu-genmon.sh`

* Make sure you add the **genmon plugin** in xfce panel.

Enter genmon properties:

 * In the **Command** field, put : /home/user'sname/path-to-directory/cpu-genmon/cpu_usage.sh
 * In the **Period** field, put: 1 second
 * Hit **Save**
 * You are good to go.
 * Also, fill in the right directory at line 45 of cpu-genmon.sh, concerining the path of the image, for instance, 

`echo "<img>/home/user/dirpath/cpu-genmon/x4cyan/$IMG</img>`
