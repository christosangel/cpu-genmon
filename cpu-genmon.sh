#! /bin/bash
#┏━╸┏━┓╻ ╻   ┏━╸┏━╸┏┓╻┏┳┓┏━┓┏┓╻
#┃  ┣━┛┃ ┃╺━╸┃╺┓┣╸ ┃┗┫┃┃┃┃ ┃┃┗┫
#┗━╸╹  ┗━┛   ┗━┛┗━╸╹ ╹╹ ╹┗━┛╹ ╹
#A bash script written by Christos Angelopoulos, January 2023
#An applet for xfce DE, to be used with panel applet genmon
cache=/tmp/cpubarscache0
stats="$(grep cpu /proc/stat)"
if [ ! -f $cache ]
then
 echo "$stats" > "$cache"
	sleep 1
	stats="$(grep cpu /proc/stat)"
fi
old=$(cat "$cache")
TT="$(echo "$stats"&& echo "$old")"
MM="$(echo "
DISK : "$(df |grep /dev/sda2|awk '{print $5}'))"
R0="$(echo "RAM :"$(free -h|grep "Mem"|awk '{print $3}'))"
R="$(echo "Occupied RAM: "$(free -h|grep "Mem"|awk '{print $3}'))"
S="$(sensors|grep Core|sed 's/+/ /g;s/        / /g;s/°C.*$/ °C/g')"
RS="$R0
────────────
$S"
echo -e "<tool>
$MM
────────────
$RS
────────────"
UNITS=$(( $(grep cpu /proc/stat|wc -l) - 1 ))
TOTALCPU="$(echo "$TT"|grep 'cpu '| awk -v RS="" '{printf "%.4f\n", ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}'|cut -b -4)"
u=0
while [ $u -lt $UNITS ]
do
	CPU[$u]="$(echo "$TT"|grep 'cpu'$u| awk -v RS="" '{printf "%.0f\n", ($13-$2+$15-$4)*10/($13-$2+$15-$4+$16-$5)}'|sed 's/-//')"
	if [ ${CPU[$u]} -gt 9 ];then	CPU[$u]=9;fi
	CPUX[$u]="$(echo "$TT"|grep 'cpu'$u| awk -v RS="" '{printf "%.3f\n", ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}'|cut -b -4)"%
	echo "cpu"$u" : " ${CPUX[$u]}
	((u++))
done
echo "$stats" > "$cache"
echo "────────────</tool><click>gnome-system-monitor</click>
<txt>$TOTALCPU%" "</txt>"
b=0
IMG=${CPU[0]}${CPU[1]}${CPU[2]}${CPU[3]}
echo "<img>/home/christos/git/cpu-genmon/x4cyan/$IMG</img>
"
